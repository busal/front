#!/bin/bash

# Vars
export DOMAIN=busal.es

docker stack deploy -c docker-compose.yml busal-front

# --- Restart portainer service
# docker service update portainer_portainer --force