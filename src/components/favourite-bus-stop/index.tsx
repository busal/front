import React, { Component } from 'react'
import TimeBall from '../time-ball'
import {ListItem} from 'material-ui/List'
import Avatar from 'material-ui/Avatar'
import IconButton from 'material-ui/IconButton'
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert'
import IconMenu from 'material-ui/IconMenu'
import MenuItem from 'material-ui/MenuItem'
import {grey400} from 'material-ui/styles/colors'
import Remote from '../../Remote'
import Local from '../../Local'

type PropTypes = {
  arrival?: LineStopsInterface,
  numStop: number,
  numLine: number,
}
type StateTypes = {
  fav: boolean,
  loading: boolean,
  numStop: number,
  arrival: LineStopsInterface|undefined,
}

class BusStop extends Component<PropTypes, StateTypes>{
  state: StateTypes

  constructor(props: PropTypes) {
    super(props)

    this.state = {
      fav: false,
      loading: true,
      numStop: 0,
      arrival: this.props.arrival,
    }
  }

  componentDidMount(){
    this.fetchStopData()
    setInterval(this.reloadStopData.bind(this), 30000)
  }

  render() {
    const { arrival, fav } = this.state
    let isHidden, rightIconMenu = null
    isHidden = !fav

    if (isHidden) {
      return <div />
    }

    if (!arrival) {
      return (
        <div className="text-centered">
          No hay servicio
        </div>
      )
    }

    const iconButtonElement = (
      <IconButton
        touch={true}
        tooltip="more"
        tooltipPosition="bottom-left"
      >
        <MoreVertIcon color={grey400} />
      </IconButton>
    )

    rightIconMenu = (
      <IconMenu iconButtonElement={iconButtonElement}>
        { !fav && <MenuItem onClick={this.addToFavs.bind(this)}>Añadir a favoritos</MenuItem> }
        { fav && <MenuItem onClick={this.removeFromFavs.bind(this)}>Quitar de favoritos</MenuItem> }
        <MenuItem onClick={this.reloadStopData.bind(this)}>Recargar</MenuItem>
      </IconMenu>
    )

    return(
      <ListItem
        primaryText={ arrival.stopName || 'Dirección parada' }
        secondaryText={ arrival.directionName }
        leftAvatar={<Avatar icon={<TimeBall minutes={arrival.minutesToArrival} />} />}
        rightIconButton={rightIconMenu}
      />
    )
  }

  removeFromFavs() {
    const { arrival } = this.state

    if (!arrival) {
      return
    }

    Local.removeBusStopAsFavorite(arrival.lineRef, this.props.numStop)
    this.setState({
      fav: false
    })
  }

  addToFavs() {
    const { arrival } = this.state

    if (!arrival) {
      return
    }

    Local.saveBusStopAsFavorite(arrival.lineRef, this.props.numStop)
    this.setState({
      fav: true
    })
  }

  reloadStopData() {
    // this.setState({
    //   loading: true
    // })

    this.fetchStopData()
  }

  fetchStopData() {
    Remote.getStop(this.props.numStop).then((stop: StopInterface) => {
      const matchArrivals = stop.lines.filter((arrival) => (
        arrival.lineRef === this.props.numLine)
      )
      this.setState({
        arrival: matchArrivals[0],
        fav: Local.isBusStopFavorite(this.props.numLine, this.props.numStop),
        loading: false
      })
    })
  }

}

export default BusStop
