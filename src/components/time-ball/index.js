import React, { Component } from 'react';
import muiThemeable from 'material-ui/styles/muiThemeable';

class TimeBall extends Component{

  styles = {
    busTime: {
      display: 'inline-flex',
      width: '40px',
      height: '40px',
      alignItems: 'center',
      justifyContent: 'center',
      flexDirection: 'column',
      color: 'white',
      backgroundColor: ''
    },
    busTimeNumber: {
      fontSize: '20px',
      display: 'block'
    },
    busTimeText: {
      fontSize: '8px',
      display: 'block'
    }
  }

  render() {
    let { minutes } = this.props;

    if (parseInt(minutes) === 0) {
      minutes = '>>>'
    }

    // this.styles.busTime.backgroundColor = this.props.muiTheme.palette.accent1Color;
    return(
      <div style={this.styles.busTime}>
        <span style={this.styles.busTimeNumber}>{ minutes }</span>
        {minutes !== '>>>' && <span style={this.styles.busTimeText}>mins</span>}
      </div>
    );
  }

};

export default muiThemeable()(TimeBall);
