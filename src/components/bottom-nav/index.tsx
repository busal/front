import React, {Component} from 'react'
import FontIcon from 'material-ui/FontIcon'
import {BottomNavigation, BottomNavigationItem} from 'material-ui/BottomNavigation'
import Paper from 'material-ui/Paper'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import { Link } from 'react-router-dom'

const busIcon = <FontIcon className="material-icons">directions_bus</FontIcon>
const favoritesIcon = <FontIcon className="material-icons">stars</FontIcon>
const infoIcon = <FontIcon className="material-icons">info</FontIcon>

class BottomNav extends Component<{location: any}, {selectedIndex: number}> {
  state = {
    selectedIndex: 0,
  }

  select = (index: number) => this.setState({selectedIndex: index})

  componentDidMount() {
    if (this.props.location.pathname === '/acerca-de') {
      this.select(2)
    } else if (this.props.location.pathname === '/') {
      this.select(0)
    } else {
      this.select(1)
    }
  }

  render() {
    return (
      <MuiThemeProvider>
        <Paper zDepth={1}>
          <BottomNavigation selectedIndex={this.state.selectedIndex}>
            <BottomNavigationItem
              containerElement={<Link to="/" className="text-centered"/>}
              label="Inicio"
              icon={favoritesIcon}
              onClick={() => this.select(0)}
            />
            <BottomNavigationItem
              containerElement={<Link to="/lineas" className="text-centered"/>}
              label="Líneas"
              icon={busIcon}
              onClick={() => this.select(1)}
            />
            <BottomNavigationItem
              containerElement={<Link to="/acerca-de" className="text-centered"/>}
              label="Acerca de"
              icon={infoIcon}
              onClick={() => this.select(2)}
            />
          </BottomNavigation>
        </Paper>
      </MuiThemeProvider>
    )
  }
}

export default BottomNav
