import React, { Component } from 'react'
import TimeBall from '../time-ball'
import {ListItem} from 'material-ui/List'
import Avatar from 'material-ui/Avatar'
import LinearProgress from 'material-ui/LinearProgress'

type PropTypes = {
  arrival?: LineStopsInterface,
}
type StateTypes = {
  loading: boolean,
  arrival: LineStopsInterface|undefined,
}

class BusStop extends Component<PropTypes, StateTypes>{

  constructor(props: PropTypes) {
    super(props)

    this.state = {
      arrival: this.props.arrival,
      loading: true
    }
  }

  componentDidMount(){
    const { arrival } = this.props

    this.setState({
      arrival,
      loading: false,
    })
  }

  render() {
    const {arrival, loading} = this.state

    if (loading)
      return (<div className="linear-loading-panel">
        <LinearProgress mode="indeterminate" />
      </div>)

    if (!arrival)
      return (<div></div>)

    return(
      <ListItem
        primaryText={ `Línea ${arrival.lineRef}` }
        secondaryText={ arrival.directionName }
        leftAvatar={<Avatar icon={<TimeBall minutes={arrival.minutesToArrival} />} />}
      />
    )
  }

}

export default BusStop
