import React, { ReactNode, SFC } from 'react';
import muiThemeable from 'material-ui/styles/muiThemeable';
import { MuiTheme } from 'material-ui/styles';

type Props = {
  muiTheme?: MuiTheme
  title?: string
  children?: ReactNode
}

const SectionWithTitle: SFC<Props> = ({ muiTheme, title, children }) => (
  <section style={{
    margin: '10px 20px',
    border: `1px solid ${muiTheme!.palette!.borderColor}`,
    borderRadius: 3
  }}>
    <h1 style={{
      color: muiTheme!.palette!.alternateTextColor,
      backgroundColor: muiTheme!.palette!.primary2Color,
      padding: 8,
    }}>
      {title}
    </h1>
    <div>
      {children}
    </div>
  </section>
);

export default muiThemeable()(SectionWithTitle);
