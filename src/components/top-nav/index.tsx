import React, { SFC } from 'react'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import { Link } from 'react-router-dom'
import IconButton from 'material-ui/IconButton'
import NavigationArrowBack from 'material-ui/svg-icons/navigation/arrow-back'
import AppBar from 'material-ui/AppBar'

const TopNav: SFC<{title: string, backRoute?: string}> = ({ title, backRoute }) => (
    <div className="site-header">
      <MuiThemeProvider>
        <AppBar
          title={ title }
          iconElementLeft={backRoute ? <IconButton containerElement={<Link to={backRoute}/>} ><NavigationArrowBack /></IconButton> : undefined}
          showMenuIconButton={backRoute ? true : false }
        />
      </MuiThemeProvider>
    </div>
  )


export default TopNav
