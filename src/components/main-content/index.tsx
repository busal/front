import React, { FC } from 'react'
import Paper from 'material-ui/Paper'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'


const MainContent: FC = (props) => (
  <MuiThemeProvider>
    <Paper zDepth={0}>
      {props.children}
    </Paper>
  </MuiThemeProvider>
)

export default MainContent
