import React, { Component } from "react";
import { Link } from "react-router-dom";
import { List, ListItem } from "material-ui/List";
import ActionGrade from "material-ui/svg-icons/action/grade";
import TopNav from "../components/top-nav";
import { Tabs, Tab } from "material-ui/Tabs";

import Remote from "../Remote";

type PropTypes = { match: any };
type StateTypes = {
  line?: string;
  dir22?: string;
  dir11?: string;
  stops_dir_11: Array<StopInterface>;
  stops_dir_22: Array<StopInterface>;
};

class Line extends Component<PropTypes, StateTypes> {
  state = {
    dir11: "",
    dir22: "",
    stops_dir_11: [],
    stops_dir_22: [],
  };

  componentWillMount() {
    const numLine = this.props.match.params.numLine;

    Remote.getLine(numLine).then((line: LineInterface) => {
      const directions = line.name.split("-");

      this.setState({
        line: line.name,
        dir22: directions.length > 1 ? directions[0] : "Vuelta",
        dir11: directions.length > 1 ? directions[1] : "Ida",
      });
    });

    Remote.getStopsBusLineByDirection(this.props.match.params.numLine, 11).then(
      (stops: Array<StopInterface>) => {
        this.setState({
          stops_dir_11: stops,
        });
      }
    );

    Remote.getStopsBusLineByDirection(this.props.match.params.numLine, 22).then(
      (stops: Array<StopInterface>) => {
        this.setState({
          stops_dir_22: stops,
        });
      }
    );
  }

  renderStopList(dirName: string, stops: Array<StopInterface>) {
    if (stops) {
      const stopList = stops.map((stop, index) => (
        <ListItem
          key={index}
          primaryText={stop.name}
          secondaryText={`Parada número ${stop.ref}`}
          leftIcon={<ActionGrade />}
          containerElement={
            <Link
              to={`/linea/${this.props.match.params.numLine}/parada/${stop.ref}`}
            />
          }
        />
      ));

      return (
        <Tab
          label={
            <div>
              <span style={{ display: "block" }}>Dirección</span>
              <span style={{ display: "block" }}>{dirName}</span>
            </div>
          }
          style={{ fontSize: "10px" }}
        >
          <List>{stopList}</List>
        </Tab>
      );
    } else {
      return <div />;
    }
  }

  render() {
    const { stops_dir_11, stops_dir_22, dir11, dir22 } = this.state;

    return (
      <div>
        <TopNav
          title={`Línea ${this.props.match.params.numLine}`}
          backRoute="/lineas"
        />
        <Tabs>
          {stops_dir_11 && this.renderStopList(dir11, stops_dir_11)}
          {stops_dir_22 && this.renderStopList(dir22, stops_dir_22)}
        </Tabs>
      </div>
    );
  }
}

export default Line;
