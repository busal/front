import React, { FC, useState, useEffect } from 'react'
import TopNav from '../components/top-nav'
import { Link } from 'react-router-dom'
import Subheader from 'material-ui/Subheader'
import {List, ListItem} from 'material-ui/List'

import ActionGrade from 'material-ui/svg-icons/action/grade'

import Remote from '../Remote'

const Lines: FC<{lines: LineInterface[]}> = () => {
  const [lines, setLines] = useState<LineInterface[]>([]);

  useEffect(() => {
    Remote.getAllBusLines()
      .then((lines: Array<LineInterface>) => {
        setLines(lines)
      })
  }, [])

  const lineRows = lines.map((line: LineInterface, idx: number) => (
    <ListItem
      key={idx}
      primaryText={ `Línea ${line.ref}`}
      secondaryText={line.name}
      leftIcon={<ActionGrade />}
      containerElement={<Link to={`/linea/${line.ref}`} />}
    />
  ))

  return (
    <div>
      <TopNav title="Busal" />
      <List>
        <Subheader>Líneas de autobus urbano</Subheader>
        {lineRows}
      </List>
    </div>
  )
}

export default Lines
