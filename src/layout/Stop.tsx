import React, { Component } from 'react';
import TopNav from '../components/top-nav';
import Subheader from 'material-ui/Subheader';
import BusStop from '../components/bus-stop';
import FlatButton from 'material-ui/FlatButton';
import {List} from 'material-ui/List';
import Divider from 'material-ui/Divider';
import { Link } from 'react-router-dom';
import CircularProgress from 'material-ui/CircularProgress';
import Local from '../Local';
import Remote from '../Remote';


class Stop extends Component<{ match: any }, {loading: boolean, fav: boolean, stop?: StopInterface}> {
  state: {
    loading: boolean;
    fav: boolean;
    stop?: StopInterface
  } = {
    loading: true,
    fav: false,
    stop: undefined,
  }

  componentWillMount() {
    Remote.getStop(this.props.match.params.numStop).then((stop: StopInterface) => {
      this.setState({
        stop: stop,
        loading: false,
        fav: this.props.match.params.numLine ? Local.isBusStopFavorite(this.props.match.params.numLine, this.props.match.params.numStop) : false
      });
    });
  }

  removeFromFavs() {
    Local.removeBusStopAsFavorite(this.props.match.params.numLine, this.props.match.params.numStop);
    this.setState({
      fav: false
    });
  }

  addToFavs() {
    Local.saveBusStopAsFavorite(this.props.match.params.numLine, this.props.match.params.numStop);
    this.setState({
      fav: true
    });
  }

  renderViewAllButton(numLine: string, numStop: string) {
    if (numLine) {
      return (
        <div>
          <Divider />
          <FlatButton
            label="Ver todas las líneas"
            fullWidth={true}
            containerElement={<Link to={`/parada/${numStop}`} />}
          />
        </div>
      )
    }
  }

  renderFavButton(numLine: string, numStop: string) {
    if (numLine) {
      return (
        <div>
          <Divider />
          <FlatButton
            label={this.state.fav ? 'Quitar de favoritos' : 'Añadir a favoritos'}
            fullWidth={true}
            onClick={this.state.fav ? this.removeFromFavs.bind(this) : this.addToFavs.bind(this)}
          />
        </div>
      )
    }
  }

  renderData(stop: StopInterface) {
    const { numLine, numStop } = this.props.match.params;

    const nexBuses = stop.lines.map((line: LineStopsInterface, idx) => {
      if (!numLine || (numLine && numLine === line.lineRef)) {
        return (<BusStop key={idx} arrival={line} />);
      }
      return null
    });

    return (
      <div>
        {this.renderTopNav(stop, numLine)}
        <List>
          { this.renderSubtitle(stop, numLine) }
          {nexBuses}
        </List>
        {this.renderViewAllButton(numLine, numStop)}
        {this.renderFavButton(numLine, numStop)}
      </div>
    )
  }

  renderTopNav(stop?: StopInterface, numLine?: string){
    if (numLine && stop) {
      return <TopNav title={stop.name} backRoute={`/linea/${numLine}`} />
    } else if (stop) {
      return <TopNav title={stop.name} backRoute={`/lineas`} />
    } else {
      return <TopNav title="Cargando..." backRoute={`/lineas`} />
    }
  }

  renderSubtitle(stop: StopInterface, numLine: string) {
    let title;
    if (stop.lines.length > 1) {
      title = 'Próximos autobuses';
    } else if (stop.lines.length === 1) {
      title = 'Próximo autobus'
    } else {
      title = 'No hay servicio'
    }

    if (numLine) {
      title += ' en la línea ' + numLine;
    }

    return (<Subheader>
      {title}
    </Subheader>);
  }

  renderLoading() {
    return (
      <div>
        {this.renderTopNav()}
        <div className="loading-panel">
          <CircularProgress size={60} thickness={7} />
        </div>
      </div>
    );
  }

  render() {
    const { stop, loading } = this.state;

    if (loading || !stop) {
      return this.renderLoading();
    } else {
      return this.renderData(stop);
    }
  }
}

export default Stop;
