import React from "react";
import TopNav from "../components/top-nav";

const About = (): JSX.Element => (
  <React.Fragment>
    <TopNav title="Acerca de busal.es" />
    <p>Creado por Jesús Merino</p>
  </React.Fragment>
);

export default About;
