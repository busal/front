import React from "react";
import FavouriteBusStop from "../components/favourite-bus-stop";
import SectionWithTitle from "../components/section-with-title";
import Subheader from "material-ui/Subheader";
import TopNav from "../components/top-nav";
import Local from "../Local";

const FavStopsList = ({
  favStops,
}: {
  favStops: StopInterface[];
}): JSX.Element => {
  if (!favStops.length) {
    return (
      <p>
        {`No has marcado ninguna parada como favorita. Para añadirlas a tus
        favoritos tienes que buscarla y pulsar el boton "Añadir a favoritos"`}
      </p>
    );
  }

  const lines = [...favStops.map((item: StopInterface) => item.numLine)];

  return (
    <React.Fragment>
      {lines.map((line) => {
        const stopsOfLine = favStops.filter((stop) => stop.numLine === line);
        const stops = stopsOfLine.map((stop) => (
          <FavouriteBusStop
            key={stop.numStop}
            numLine={line}
            numStop={stop.numStop}
          />
        ));

        return (
          <SectionWithTitle title={`Línea ${line}`} key={`${line}`}>
            {stops}
          </SectionWithTitle>
        );
      })}
    </React.Fragment>
  );
};

const Dashboard = (): JSX.Element => {
  return (
    <div>
      <TopNav title="Busal" />
      <Subheader>Tus paradas favoritas:</Subheader>
      <p>
        Estas son las paradas y líneas que has marcado como favoritas. Si dejas
        esta pantalla abierta los tiempos se recargarán cada 30 segundos.
      </p>
      <FavStopsList favStops={Local.getFavoritesBusStops()} />
    </div>
  );
};

export default Dashboard;
