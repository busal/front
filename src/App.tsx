import React, { FC } from 'react'
import MainContent from './components/main-content'

import BottomNav from './components/bottom-nav'
import './layout/style.css'


const App: FC<{ location: any }> = (props) => (
  <div>
    <div className="page-wrap">
      <MainContent>
        {props.children}
      </MainContent>
    </div>
    <div className="site-footer">
      <BottomNav {...props}/>
    </div>
  </div>
)

export default App
