interface StopInterface {
  numLine: number,
  numStop: number,
  lat: string,
  lng: string,
  name: string,
  order: string,
  ref: string
  lines: Array<LineStopsInterface>
}

interface LineStopsInterface{
  lineRef: number,
  stopName: string,
  directionRef: string,
  vehicleRef: string,
  lineName: string,
  directionName: string,
  expectedArrivalTime: string,
  aimedArrivalTime: string,
  minutesToArrival: number,
  distance: string,
  delay: number,
  location: {
    lat: string,
    lng: string
  }
}

interface LineInterface {
  name: string,
  ref: number
}

