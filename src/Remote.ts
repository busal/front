
const apiEndpoint = process.env.REACT_APP_API_HOST

function getAllBusLines(): Promise<Array<LineInterface>> {
  return fetch(`${apiEndpoint}/v1/lines`, {
    headers: {
      accept: 'application/json',
    }
  }).then(checkStatus)
    .then(parseJSON)
    .then((value: object) => Object.values(value))
}

function getLine(numLine: string): Promise<LineInterface> {
  return fetch(`${apiEndpoint}/v1/line/${numLine}`, {
    headers: {
      accept: 'application/json',
    }
  }).then(checkStatus)
    .then(parseJSON)
    .then((value: object) => Object.values(value)[0])
}

function getStopsBusLineByDirection(numLine: string, direction: number): Promise<Array<StopInterface>> {
  return fetch(`${apiEndpoint}/v1/line/${numLine}/${direction}`, {
    headers: {
      accept: 'application/json',
    }
  }).then(checkStatus)
    .then(parseJSON)
    .then((value: object) => Object.values(value))
}

function getStop(numStop: number): Promise<StopInterface> {
  return fetch(`${apiEndpoint}/v1/stop/${numStop}`, {
    headers: {
      accept: 'application/json',
    }
  }).then(checkStatus)
    .then(parseJSON)
}

function checkStatus(response: any) {
  if (response.status >= 200 && response.status < 300) {
    return response;
  }
  const error = new Error(`HTTP Error ${response.statusText}`);
  console.log(response);
  console.log(error);
  throw error;
}

function parseJSON(response: any): any {
  return response.json()
}

const Remote = { getAllBusLines, getLine, getStopsBusLineByDirection, getStop };
export default Remote;
