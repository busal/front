import React from 'react'
import {
  BrowserRouter as Router,
  Route,
  Switch
} from 'react-router-dom'
import App from './App'
import Dashboard from './layout/Dashboard'
import Lines from './layout/Lines'
import Line from './layout/Line'
import Stop from './layout/Stop'
import About from './layout/About'
import NotFound from './layout/NotFound'

const IndexRoute = (props: any) => {
  const { component: Component, ...rest } = props
  return (<Route {...rest} render={props => (<App {...props}>
    <Component {...props} />
  </App>)} />)
}

const Routes = () => (
  <Router>
      <Switch>
        <IndexRoute exact path="/" component={Dashboard} />
        <IndexRoute path="/lineas" component={Lines} />
        <IndexRoute path="/parada/:numStop" component={Stop} />
        <IndexRoute path="/linea/:numLine/parada/:numStop" component={Stop} />
        <IndexRoute path="/linea/:numLine" component={Line} />
        <IndexRoute path="/acerca-de" component={About} />
        <IndexRoute path="*" component={NotFound} />
      </Switch>
  </Router>
)

export default Routes
