import store from "store";

function findBusStop(
  numLine: number,
  numStop: number
): StopInterface | boolean {
  const favorites = store.get("busal.favs");
  if (!favorites) {
    return false;
  }

  const item = favorites.filter(
    (item: StopInterface) =>
      item.numLine === numLine && item.numStop === numStop
  );
  return item[0];
}

function saveBusStopAsFavorite(numLine: number, numStop: number): void {
  if (!isBusStopFavorite(numLine, numStop)) {
    let favorites = store.get("busal.favs");

    if (!favorites) {
      favorites = [];
    }

    favorites.push({ numLine, numStop });
    store.set("busal.favs", favorites);
  }
}

function removeBusStopAsFavorite(numLine: number, numStop: number): void {
  const favorites = store.get("busal.favs");

  const items = favorites.filter(
    (item: StopInterface) =>
      item.numLine !== numLine || item.numStop !== numStop
  );
  store.set("busal.favs", items);
}

function getFavoritesBusStops(): Array<StopInterface> {
  return store.get("busal.favs") || [];
}

function isBusStopFavorite(numLine: number, numStop: number): boolean {
  const busStop = findBusStop(numLine, numStop);
  return busStop ? true : false;
}

const Local = {
  saveBusStopAsFavorite,
  getFavoritesBusStops,
  isBusStopFavorite,
  removeBusStopAsFavorite,
};
export default Local;
